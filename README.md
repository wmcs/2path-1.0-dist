# 2path-1.0 #

2Path is a terpenoid metabolic network databases created on demand from a public workflow for in silico metabolic network reconstruction.
2Path integrates data in reaction level from several repositories of plant metabolism, filtering for terpenoid data.

### What is this repository for? ###

Here you can obtain the called 2path in its version 1.0, which is a terpenoid database modeled as a graph database.

### In the box ###

* Database dump
* Web Interface

### What else do I need? ###

* A Linux operational system
* Neo4J version 3.2.x
* Java version 8
* Internet access


### Make it go ###

First, clone this repository using:

```bash
git clone https://wmcs@bitbucket.org/wmcs/2path-1.0-dist.git
```

#### Install and configure Neo4J ####

[Download Neo4J enterpise or community edition](htpp://www.neo4j.com)

[We suggest the current version (3.4.7)](https://neo4j.com/artifact.php?name=neo4j-community-3.4.7-unix.tar.gz)

Edit the file <NEO4J_HOME>/conf/neo4j.conf

Edit the line:

```
dbms.active_database=2path-1.0
```

Uncomment the line: 

```
dbms.allow_upgrade=true
```
Restore the 2path-1.0-dist to your neo4j:

```bash
./<NEO4J_HOME>/bin/neo4j-admin load --from=<FULL_PATH>/2path-1.0.dump --database=2path-1.0 --force
```

#### Run Neo4J ####
```bash
./<NEO4J_HOME>/bin/neo4j start
```

### Exploring ###

There are two ways to explore the database:

1. Using the provided interface

2. Excecuting cypher queries (cypher skills required)


#### Using the interface ####

After restore the 2path-1.0 into neo4j and start it, just open in you browser the file index.html

#### Excecuting cypher queries ####

Many biological questions can be answered using queries. Some examples are:


```cypher
//Copaifera multijuga Hayne transcripts report
MATCH q=(t:Taxonomy{taxId:"327897"})-[]-(p:Protein)-[]-(e:Enzyme)-[]-(r:Reaction)
RETURN
p.proteinGene as transcript,
e.enzymeEC as ec,
p.proteinRecomendedName as suggestedAnnotation,
r.reactionDescription as reaction,
p.proteinGOs as GOs
```


//Copaifera multijuga network (limited to 25 results)
```cypher
MATCH
(c:Compound)-[r1]-(r:Reaction)-[r2]-(e:Enzyme)-[r3]-(p:Protein)-[r4]-
(t:Taxonomy {taxId:"327897"}) RETURN c,r,e,p,t LIMIT 25
```

//Compounds related to the Copaiba terpenoid network (limited to 10 results)
```cypher
MATCH qa=(ta:Taxonomy{taxId:"327897"})-[]-(pa:Protein)-[]-(ea:Enzyme)-[]-(ra:Reaction)-[]-(ca:Compound)
RETURN ca.compoundName as compound LIMIT 10
```

//Copaifera multijuga hayne proteins with catalityc activity
```cypher
MATCH q=(t:Taxonomy{taxId:"327897"})-[s:HAS]-(p:Protein)-[r:CATALYTIC_ACTIVITY]->(e:Enzyme)
RETURN p.proteinRecomendedName as suggestedAnnotation
```

[More about Cypher language](http://neo4j.com/developer/cypher)


### Publications ###

[Silva, W., Vilar, D., Souza, D., Walter, M. E., Brígido, M., & Holanda, M. (2016). 2Path: A terpenoid metabolic network modeled as graph database. In Bioinformatics and Biomedicine (BIBM), 2016 IEEE International Conference on (pp. 1322-1327). IEEE.](http://ieeexplore.ieee.org/document/7822709)

[Silva, W. M. C. D., Vilar, D. J., Souza, D. D. S., Walter, M. E. M. T., Holanda, M. T. D., & Brígido, M. D. M. (2017). A terpenoid metabolic network modelled as graph database. International Journal of Data Mining and Bioinformatics, 18(1), 74-90.](http://www.inderscience.com/info/inarticle.php?artid=86103)

[Esteves, G., Silva, W., Walter, M. E., Brigido, M., & Lima, F. (2018). Human-Computer Interaction Communicability Evaluation Method Applied to Bioinformatics. In World Conference on Information Systems and Technologies (pp. 1001-1008). Springer.](https://link.springer.com/chapter/10.1007/978-3-319-77712-2_95)



